const express = require('express');

const pool = require('./config');

const app = express();
app.use(express.json());

const Hello = (req, res) => {
    const version = {
        "ApiVersion" : "1.35-test",
        "Status": "Connected",
        "Url Mapping": {
            "/food" : {
                "type" : "json",
                "description" : "Returns all current foods in database."
            },
            "POST /food" : {
                "type" : "Json",
                "description" : "Post request to DB.",
                "body values" : ["dish", "country"]
            },
            "DELETE /food/<id>" : {
                "type" : "URL",
                "description" : "Delete current dish. You can delete Korean Foods if there is any."
            },
            "PUT /food/<id>" : {
                "type" : "URL",
                "description" : "Change current dish. You can change Korean Foods if there is any.."
            },
        }
    }

    res.send(version)
};

const healthCheck = (req, res) => {
  res.status(200).json({ status: 'OK' })
}

const  getFood = async (req, res) => {
  // Use a client from the pool.
  const client = await pool.connect();

  try {
    // Perform the query using the client.
    const foods = await client.query("SELECT * FROM food");
    res.status(200).json({
        status: 'sucess',
        data: foods.rows,
      });
  } finally {
    // Release the client back to the pool when done.
    client.release();
  }
};

const getFoodById = async (req, res) => {
    const reqId = parseInt(req.params.id);
    const client = await pool.connect();
    
    try {
      const food = await client.query('SELECT * FROM food WHERE id = $1', [reqId]);
  
      if (food.rows.length === 0) {
        // Return 404 if no food item is found with the specified ID
        res.status(404).json({
          status: 'not found',
          message: 'Food item not found',
        });
      } else {
        // Return the found food item
        res.status(200).json({
          status: 'success',
          requestTime: req.requestTime,
          data: food.rows[0], // Assuming you want to return only the first matching row
        });
      }
    } catch (error) {
      console.error(error);
      res.status(500).json({
        status: 'error',
        message: 'Internal server error',
      });
    } finally {
      client.release();
    }
};

const createFood = async (req, res) => {
const { dish, country } = req.body; // Assuming the request body contains dish and country

if (!dish || !country) {
    // If dish or country is missing, return a 400 Bad Request response
    return res.status(400).json({
    status: 'bad request',
    message: 'Dish and country are required fields',
    });
}

const client = await pool.connect();

try {
    // Insert a new food item into the database
    const result = await client.query('INSERT INTO food (dish, country) VALUES ($1, $2) RETURNING *', [dish, country]);

    // Return the created food item
    res.status(201).json({
    status: 'success',
    data: result.rows[0],
    });
} catch (error) {
    console.error(error);
    res.status(500).json({
    status: 'error',
    message: 'Internal server error',
    });
} finally {
    client.release();
}
};

const deleteFood = async (req, res) => {
    const reqId = parseInt(req.params.id);
    const client = await pool.connect();
  
    try {
      // Check if the food item with the specified ID exists
      const existingFood = await client.query('SELECT * FROM food WHERE id = $1', [reqId]);
  
      if (existingFood.rows.length === 0) {
        // Return 404 if no food item is found with the specified ID
        return res.status(404).json({
          status: 'not found',
          message: 'Food item not found',
        });
      }
  
      // Delete the food item with the specified ID
      await client.query('DELETE FROM food WHERE id = $1', [reqId]);
  
      // Return a success response
      res.status(200).json({
        status: 'success',
        message: 'Food item deleted successfully',
      });
    } catch (error) {
      console.error(error);
      res.status(500).json({
        status: 'error',
        message: 'Internal server error',
      });
    } finally {
      client.release();
    }
};

const updateFood = async (req, res) => {
    const reqId = parseInt(req.params.id);
    const { dish, country } = req.body;
    const client = await pool.connect();

try {
    // Check if the food item with the specified ID exists
    const existingFood = await client.query('SELECT * FROM food WHERE id = $1', [reqId]);

    if (existingFood.rows.length === 0) {
    // Return 404 if no food item is found with the specified ID
    return res.status(404).json({
        status: 'not found',
        message: 'Food item not found',
    });
    }

    // Check if 'dish' is provided in the request body
    if (dish === undefined || dish === null) {
    return res.status(400).json({
        status: 'bad request',
        message: 'Dish is required for the update',
    });
    }

    // Update the food item with the specified ID
    const updatedFood = await client.query(
    'UPDATE food SET dish = $1, country = $2 WHERE id = $3 RETURNING *',
    [dish, country, reqId]
    );

    // Return the updated food item
    res.status(200).json({
    status: 'success',
    message: 'Food item updated successfully',
    data: updatedFood.rows[0],
    });
} catch (error) {
    console.error(error);
    res.status(500).json({
    status: 'error',
    message: 'Internal server error',
    });
} finally {
    client.release();
}
};

app.route('/').get(Hello)
app.route('/healthCheck').get(healthCheck)
app.route('/food').get(getFood).post(createFood)
app.route('/food/:id').get(getFoodById).delete(deleteFood).put(updateFood);
module.exports = app;
