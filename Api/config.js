const { Pool } = require("pg");
const { use } = require("./app");
var env = require('env-variable')();


/* const credentials = {
  user: process.env.user,
  host: process.env.host,
  database: process.env.database,
  password: process.env.password,
  port: process.env.port,
}; */

const user = process.env.POSTGRES_USER
const password = process.env.POSTGRES_PASSWORD
const host = process.env.POSTGRES_HOST
const port = process.env.POSTGRES_PORT
const db = process.env.POSTGRES_DB

const connectionString = `postgresql://${user}:${password}@${host}:${port}/${db}`

const pool = new Pool({
  connectionString: connectionString
});

async function poolDemo() {
  const client = await pool.connect();

  try {
    const foods = await client.query("SELECT * FROM food");

    return foods;
  } finally {
    client.release();
  }
}

// Use a self-calling function so we can use async / await.
/* (async () => {
  try {
    const poolResult = await poolDemo();
    console.log(poolResult.rows);
  } finally {
    // Close the pool when done using it (you may not want to do this if your app is long-running).
    await pool.end();
  }
})(); */

module.exports = pool