const app = require('./app');

const port = process.env.web_port || 3000;
app.listen(port, () => {
  console.log("I hope so.")
  console.log(process.env.POSTGRES_USER)
  console.log(`App running on port ${port}.`);
});
