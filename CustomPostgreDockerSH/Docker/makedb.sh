#!/bin/bash

# PostgreSQL commandsssssssss
commands=$(cat <<EOF
CREATE DATABASE foodie;
\c foodie

CREATE ROLE default_user WITH LOGIN PASSWORD 'default_password';
ALTER ROLE default_user CREATEDB;
GRANT ALL PRIVILEGES ON DATABASE foodie TO default_user;
GRANT ALL PRIVILEGES ON DATABASE foodie TO postgres;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO default_user;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO default_user;

\q

EOF

)

commands2=$(cat <<EOF

CREATE TABLE food (
  ID SERIAL PRIMARY KEY,
  Dish VARCHAR(30) NOT NULL,
  Country VARCHAR(30) NOT NULL
);

\q

EOF

)

echo "$commands" | psql -d postgres -U postgres

sleep 5

echo "$commands2" | psql -d foodie -U postgres
